# Helm repository 
for serving builded charts from and for another projects
index.yaml is updated automatically

# Usage
just add helm repo<br>
$ helm repo add optima_public https://optima_public.gitlab.io/helm_repo/

# Notice
some of builded images which are used in those charts are not PUBLIC and should require valid imagePullSecrets 
(Gitlab tokens) as kubernetes secrets

